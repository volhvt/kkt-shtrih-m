/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   messages.h
 * Author: volhv
 *
 * Created on 22 марта 2018 г., 9:22
 */

#ifndef MESSAGES_H
#define MESSAGES_H

const char *languages[] = {
    "русский",
    "английский",
    "эстонский",
    "казахский",
    "белорусский",
    "армянский",
    "грузинский",
    "украинский",
    "киргизский",
    "туркменский",
    "молдавский"
};

#endif /* MESSAGES_H */

