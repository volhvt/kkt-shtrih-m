/***************************************************************************
                          conn.cpp  -  description
                             -------------------
    begin                : Sat Jan 12 2002
    copyright            : (C) 2002 by Igor V. Youdytsky
    email                : Pitcher@gw.tander.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <ios>
#include "conn.h"
#include "options.h"
#include "drvfr.h"
/*
#include <log4cxx/logger.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

using namespace log4cxx;
using namespace log4cxx::helpers;

extern LoggerPtr __glb_logger;
extern LoggerPtr __glb_logger_result;
*/
using namespace std;


CommandMap _commands;
void create_map_commands(){
  _commands.clear();
  // _code, _len_code, _execute_timeout, _len_min, _len_max, _len_answer_min, _len_answer_max, _title
  _commands.insert(pair<const char*, CommandDescription >("DUMP_REQUEST",
          CommandDescription("\x01",     1,  30000, 6,   6,   4,   4,   "Запрос дампа")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x02",     1,  30000, 5,   5,   37,  253, "Запрос данных")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x03",     1,  30000, 5,   5,   2,   2,   "Прерывание выдачи данных")));

  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x10",     1,  5000,  5,   5,   16,  16,  "Короткий запрос состояния ККТ")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x11",     1,  5000,  5,   5,   48,  48,  "Запрос состояния ККТ")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x12",     1,  30000, 26,  26,  3,   3,   "Печать жирной строки (шрифт 2)")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x13",     1,  30000, 5,   5,   3,   3,   "Гудок")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x14",     1,  30000, 8,   8,   2,   2,   "Установка параметров обмена")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x15",     1,  30000, 6,   6,   4,   4,   "Чтение параметров обмена")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x16",     1,  60000, 1,   1,   2,   2,   "Технологическое обнуление")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x17",     1,  30000, 46,  46,  3,   3,   "Печать стандартной строки (шрифт 1)")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x18",     1,  30000, 37,  37,  5,   5,   "Печать заголовка документа")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x19",     1,  30000, 6,   6,   3,   3,   "Тестовый прогон")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x1A",     1,  30000, 6,   6,   9,   9,   "Запрос денежного регистра")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x1B",     1,  30000, 6,   6,   5,   5,   "Запрос операционного регистра")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x1E",     1,  30000, 6,   6,   5,   5,   "Запись таблицы")));
  _commands.insert(pair<const char*, CommandDescription >("",
          CommandDescription("\x1D",     1,  30000, 6,   6,   5,   5,   "")));

}


/**********************************************************
 * Local functions & procedures                           *
 **********************************************************/

int sendNAK(void);
int sendACK(void);
int sendENQ(void);
int determsp(int);
int set_up_tty(int);
int input_timeout(int);
int composecomm(command*, int, unsigned char*);
void settimeout(int);
const char* devname(int number);
unsigned short int readbyte(int);
unsigned short int LRC(unsigned char*, int, int);

void PrintComm(command*);
void PrintAnswer(command*);

/**********************************************************
 * Local static variables                                 *
 **********************************************************/
int __glb_connected = 0;
static int devfile, fdflags;
static fd_set set; // файловый дескриптор в множестве "на чтение"
static fr_prop *prop;
static struct timeval timeout;
//static int current_command;

/**********************************************************
 * Implementation of procedures                           *
 **********************************************************/
#ifdef DEBUG

void PrintComm(command* cmd) {
    /*
    char *s;
    char *t;

    s = (char*) malloc(cmd->len * 3 * 3);
    t = (char*) malloc(5);
    memset(s, 0, cmd->len * 3 * 3);
    for (int i = 0; i < cmd->len; i++) {
        sprintf(t, "%02X ", (int) cmd->buff[i]);
        s = strcat(s, t);
    }
    //s = strcat(s,"\n");
    perror(s);
    free(s);
    free(t);
    */
    std::stringstream log_ss;
    for (int i = 0; i < cmd->len; i++) {
        log_ss << std::hex << (int) cmd->buff[i] << " ";
    }
    std::cout << "CMD:::" << log_ss.str() << std::endl;
    //LOG4CXX_DEBUG( __glb_logger, log_ss.str() );
};

void PrintAnswer(answer* ans) {
    /*
    char *s;
    char *t;

    s = (char*) malloc(ans->len * 3 * 3);
    t = (char*) malloc(5);
    memset(s, 0, ans->len * 3 * 3);
    for (int i = 0; i < ans->len; i++) {
        sprintf(t, "%02X ", (int) ans->buff[i]);
        s = strcat(s, t);
    }
    //s = strcat(s,"\n");
    perror(s);
    free(s);
    free(t);
    */
    std::stringstream log_ss;
    for (int i = 0; i < ans->len; i++) {
        log_ss << std::hex << (int) ans->buff[i] << " ";
    }
    std::cout << "ANS:::" << log_ss.str() << std::endl;
    //LOG4CXX_DEBUG( __glb_logger, log_ss.str() );
};
#endif
//--------------------------------------------------------------------------------------

void settimeout(int msec) {
    if (msec < 0) {
        msec = 0;
    }
    timeout.tv_sec = msec / MICRO_TO_MILLI;
    timeout.tv_usec = (msec - timeout.tv_sec * MICRO_TO_MILLI) * MICRO_TO_MILLI;
    //LOG4CXX_DEBUG( __glb_logger, " = set timeout [ " << std::dec << timeout.tv_sec << " sec : " << timeout.tv_usec << " usec ]" );
}
//--------------------------------------------------------------------------------------

int opendev(fr_prop *f) {
    //perror(" before opendev");
    prop = f;
    if (prop->ComPortString) {
        //perror(" ComPortString before open");
        devfile = open(prop->ComPortString, O_NOCTTY | O_NONBLOCK | O_RDWR, 0);
        //perror(" ComPortString after open");
    } else {
        int countTires = 0;
        //perror(" ComPortNumber before wile open");
        while ((devfile = open(devname(prop->ComPortNumber), O_NOCTTY | O_NONBLOCK | O_RDWR, 0)) < 0) {
            if (errno != EINTR) {
                //perror(" ComPortNumber after wile open");
                return -1;
            } else {
                countTires++;
                if (countTires > 10) {
                    //perror(" ComPortNumber after wile open 2");
                    return -1;
                }
            }
        }
    }
    //perror(" middle opendev");
    //while ((devfile = open(devname(prop->ComPortNumber), O_NONBLOCK | O_RDWR, 0)) < 0)
    //if (errno != EINTR) return -1;
    if ((fdflags = fcntl(devfile, F_GETFL)) == -1 || fcntl(devfile, F_SETFL, fdflags & ~O_NONBLOCK) < 0) {
        return -1;
    }
    //perror(" before set_up_tty");
    set_up_tty(devfile);
    //perror(" after set_up_tty");
    FD_ZERO(&set); // очищаем множество "на чтение"
    FD_SET(devfile, &set); // помещаем дескриптор файла устройства во множество "на чтение"
    //perror(" before settimeout");
    settimeout(prop->Timeout);
    //perror(" after settimeout");
    return 1;
}
//--------------------------------------------------------------------------------------

int set_up_tty(int tty_fd) {
    int speed;
    struct termios tios;

    if (tcgetattr(tty_fd, &tios) < 0) return -1;

    tios.c_cflag &= ~(CSIZE | CSTOPB | PARENB /*| CLOCAL*/); // Размер символа | Посылать один стоповый бит | Запретить контроль четности | линия не является локальной
    tios.c_cflag |= CS8 | CREAD | HUPCL; // 8 бит | Разрешить прием | Разъединить линию при последнем закрытии файла

    tios.c_cflag &= ~CRTSCTS; // disable hardware flow control
    
    tios.c_iflag = IGNBRK | IGNPAR; //
    tios.c_oflag = 0;
    tios.c_lflag = 0;
    tios.c_cc[VMIN] = 0; //минимальное количество символо для чтения
    tios.c_cc[VTIME] = 0;
    //tios.c_cc[VTIME] = prop->Timeout/10; //минимальное время ожидания сек.

    

    speed = LineSpeedVal[prop->BaudRate];
    if (speed) {
        cfsetospeed(&tios, speed);
        cfsetispeed(&tios, speed);
    }

    if (tcsetattr(tty_fd, TCSAFLUSH, &tios) < 0) return -1;

    return 1;
}
//--------------------------------------------------------------------------------------

int determsp(int spval) {
    for (int i = 0; i < 7; i++) if ((unsigned) spval == LineSpeedVal[i]) return i;
    return 0;
}
//--------------------------------------------------------------------------------------

int closedev(void) {
    //perror(" closedev");
    return close(devfile);
}
//--------------------------------------------------------------------------------------

void auto_increase_timeout()
{
    if( __glb_args.use_auto_timeout && __glb_args.timeout + __glb_args.step_auto_timeout <= __glb_args.max_auto_timeout  ){
        __glb_args.timeout += __glb_args.step_auto_timeout;
        //std::stringstream ss;
        //LOG4CXX_DEBUG( __glb_logger, "~~~~~~~ AUTO change timeout from " << prop->Timeout << " to " << __glb_args.timeout << " msec ~~~~~~~" );
        //std::cout << ss.str() << std::endl;
        //perror(ss.str().c_str());
        prop->Timeout = __glb_args.timeout;
    }
}

//--------------------------------------------------------------------------------------

int input_timeout(int msec) {
    if (msec < 0) {
        msec = 0;
    }
    settimeout(msec);
    //LOG4CXX_DEBUG( __glb_logger, "@ start wait " );
    __glb_connected = TEMP_FAILURE_RETRY(select(FD_SETSIZE, &set, NULL, NULL, &timeout));
    //LOG4CXX_DEBUG( __glb_logger, "@ end wailt with result: [" << __glb_connected << "]" );
    return __glb_connected;
}

//--------------------------------------------------------------------------------------

unsigned short int LRC(unsigned char *str, int len, int offset = 0) {
    int i;
    unsigned char *ptr;
    unsigned char ch = 0;

    ptr = str + offset;
    for (i = 0; i < len; i++)ch ^= ptr[i];
    return ch;
}
//--------------------------------------------------------------------------------------

int sendNAK(void) {
    //LOG4CXX_DEBUG( __glb_logger, "/=> send NAK" );
    //perror("/=> send NAK");
    char buff[2];
    buff[0] = NAK;
    return write(devfile, buff, 1);
}
//--------------------------------------------------------------------------------------

int sendACK(void) {
    //LOG4CXX_DEBUG( __glb_logger, "/=> send ACK" );
    //perror("/=> send ACK");
    char buff[2];
    buff[0] = ACK;
    return write(devfile, buff, 1);
}
//--------------------------------------------------------------------------------------

int sendENQ(void) {
    //LOG4CXX_DEBUG( __glb_logger, "/=> send ENQ" );
    //perror("/=> send ENQ");
    char buff[2];
    buff[0] = ENQ;
    return write(devfile, buff, 1);
}
//--------------------------------------------------------------------------------------

int composecomm(command *cmd, unsigned int comm, parameter *param) {
    //LOG4CXX_DEBUG( __glb_logger, "  & composecomm " );
    //LOG4CXX_DEBUG( __glb_logger, "  &     sizeof command " << std::dec << param->cmd_len );
    //current_command = comm;
    int len;
    // TODO )
    if( comm == WRITE_TABLE || 
        comm == GET_BUFFER_SIZE_L || 
        comm == READ_BLOCK_FROM_BUFFER_L ||
        comm == GET_FN_STATUS_L 
    ){
        len = param->len;
    } else {
        len = commlen[comm];
    }
    //LOG4CXX_DEBUG( __glb_logger, "  &     param->len " << std::dec << param->len );
    if (len >= 5 && param->len > (len - 5)) {
        param->len = len - 5;
    }
    //LOG4CXX_DEBUG( __glb_logger, "  &     param->len " << std::dec << param->len );
    cmd->buff[0] = STX; // begin comman
    cmd->buff[1] = len; // length of mesage without 0,1 and LRC bytes

    compose_cmd(comm,  param->cmd_len, cmd->buff + 2);
    //memcpy(cmd->buff + 2, &cmdCh, param->cmd_len);// command value
    
    //LOG4CXX_DEBUG( __glb_logger, "  & comm " );
    //std::cout << std::dec << param->cmd_len << " | " << cmdCh << " [" << std::hex << comm << " | " << std::hex << "<\xff>" << "<\x01>] "  << std::dec << comm << std::endl;
    if (len >= 5) {
        memcpy(cmd->buff + (2 + param->cmd_len), &param->pass, sizeof (int)); // command password
        //LOG4CXX_DEBUG( __glb_logger, "  & pass" );
        if (param->len > 0){
            memcpy(cmd->buff + (2 + param->cmd_len + sizeof (int)), param->buff, param->len); // command body
            //LOG4CXX_DEBUG( __glb_logger, "  & body" );
        }
    };
    
    
    
    cmd->buff[len + 2] = LRC(cmd->buff, len + 1, 1); // 
    //LOG4CXX_DEBUG( __glb_logger, "  & LRC" );
    cmd->len = len + 3; // length
    //LOG4CXX_DEBUG( __glb_logger, "  & len" );
    return 1;
}

//--------------------------------------------------------------------------------------

void compose_cmd(unsigned int comm, unsigned int comm_len, unsigned char *cmd_ch)
{
    int size_n = sizeof comm;
    int _size_n = size_n;
    int cur_num = 0;
    //unsigned char cmd_ch[size_n];
    //LOG4CXX_DEBUG( __glb_logger, "  compose_cmd from " << comm << " ["<< std::dec << comm << "]" << std::endl << "  " );
    for(int j=0; size_n-->0; j++){
        if( _size_n - j <= comm_len ){
            cmd_ch[cur_num] = (comm>>(size_n*8))&0xff;
            //LOG4CXX_DEBUG( __glb_logger, j << " : " << cmd_ch[cur_num] );
            cur_num ++;
        }
    }
    //std::cout << std::endl;
}

//--------------------------------------------------------------------------------------

unsigned short int readbyte(int msec = prop->Timeout) {
    unsigned char readbuff[2] = "";
    if (input_timeout(msec) == 0) {
        return -1;
    }
    if (read(devfile, readbuff, 1) > 0) {
        return (unsigned int) readbuff[0];
    } else return 0;
}
//--------------------------------------------------------------------------------------

int readbytes(unsigned char *buff, int len) {
    int i;
    for (i = 0; i < len; i++) {
        if (input_timeout(prop->Timeout) == 1) {
            if (read(devfile, buff + i, 1) == -1) {
                return -1;
            }
        }
    };
    return len;
}
//--------------------------------------------------------------------------------------

int checkstate(void) {
    short int repl;
    sendENQ();
    repl = readbyte(prop->Timeout*2); // подождать N*2 мс для чтения
    if (__glb_connected == 0){
        return -2;
    }
    switch (repl) {
        case NAK:
            //LOG4CXX_DEBUG( __glb_logger, " ----------- NAK wait CMD " );
            break;
        case ACK:
            //LOG4CXX_DEBUG( __glb_logger, " ----------- ACK generate ACK " );
            break;
        default:
            repl = -1;
            break;
    };
    usleep(prop->Timeout*MICRO_TO_MILLI); //подождать N мс перед следующей отправкой
    return repl;
};

//--------------------------------------------------------------------------------------

int before_send_command(int comm){
    //LOG4CXX_DEBUG( __glb_logger, ": before_send_command -----> 0x" << std::hex << comm );
    //LOG4CXX_DEBUG( __glb_logger, ":       * CHECK STATE DEVICE" );
    answer a;
    short int tries = 1;
    bool flag = false;
    while (tries <= __glb_args.max_connect_tries && !flag) {
        //LOG4CXX_DEBUG( __glb_logger, ">>       tries from MAX  : " << std::dec << tries << " / " << std::dec << __glb_args.max_connect_tries );
        int state = checkstate();
        switch (state) {
            case ACK:
                //LOG4CXX_DEBUG( __glb_logger, ":       ACK(device generate answer) 0x"<< std::hex << state );
                //LOG4CXX_DEBUG( __glb_logger, ":       START readanswer " );
                readanswer(comm,&a);
                //clearanswer();
                flag = true;
                break;
            case NAK:
                //LOG4CXX_DEBUG( __glb_logger, ":       NAK(device await next command) 0x"<< std::hex << state );
                flag = true;
                break;
            case -1:
            default:
                tries++;
                break;
        };
    };
    if (!flag){
        //LOG4CXX_DEBUG( __glb_logger, ">>       FAIL[no connection with device] " );
        return -10;
    }
    //LOG4CXX_DEBUG( __glb_logger, ">>       SUCCESS " );
    return 0;
}

//--------------------------------------------------------------------------------------

int sendcommand(unsigned int comm, parameter *param) {
    //LOG4CXX_DEBUG( __glb_logger, ">> sendcommand -----> 0x" << std::hex << comm );
    short int repl, tries;
    int result;
    command cmd;
    //perror("ошибка перед composecomm ");
    //LOG4CXX_DEBUG( __glb_logger, ">> !!! reset errno !!!" );
    errno = 0;
    composecomm(&cmd, comm, param);
    //perror("ошибка после composecomm ");
#ifdef DEBUG
    PrintComm(&cmd);
#endif
    result = before_send_command(comm);
    if(  result != 0 ){
        return result;
    }

    //LOG4CXX_DEBUG( __glb_logger, ">>       * WRITE CMD TO DEVICE " );
    for (tries = 1; tries <= __glb_args.max_tries; tries++) {
        //LOG4CXX_DEBUG( __glb_logger, ">>       tries from MAX  : " << std::dec << tries << " / " << std::dec << __glb_args.max_tries );
        result = write(devfile, cmd.buff, cmd.len);
        if (result != -1) {
            //LOG4CXX_DEBUG( __glb_logger, ">>       SUCCESS WRITE TO DEVICE " );
            //LOG4CXX_DEBUG( __glb_logger, ">>        usleep [ " << std::dec << (prop->Timeout * 2 * MICRO_TO_MILLI) << " usec ] for write to device " );
            usleep(prop->Timeout * 2 * MICRO_TO_MILLI );
            
            //std::cout << ">>        usleep [ " << std::dec << (prop->Timeout * ( cmd.len * 2 ) * MICRO_TO_MILLI) << " usec ] for write to device " << std::endl;
            //usleep(prop->Timeout/2 * ( cmd.len * 2 ) * MICRO_TO_MILLI);
            
            //perror(">>      * READ CMD STATE FROM DEVICE with timeout");
            //std::cout << ">>      * READ CMD STATE FROM DEVICE with timeout " << (prop->Timeout * 1) << " msec" << std::endl;
            //repl = readbyte(prop->Timeout * 1);
            //LOG4CXX_DEBUG( __glb_logger, ">>      * READ CMD STATE FROM DEVICE with timeout " << (prop->Timeout * 2 + prop->Timeout * ( cmd.len * 2 ) ) << " msec" );
            repl = readbyte( prop->Timeout * 2 + prop->Timeout * ( cmd.len * 2 ) );
            if (__glb_connected != 0) {
                if (repl == ACK) {
                    //perror(">>      *** *** SUCCESS READ FROM DEVICE [ACK]");
                    //LOG4CXX_DEBUG( __glb_logger, ">>      *** *** SUCCESS READ FROM DEVICE [ACK] 0x" << std::hex << repl );
                    return 1;
                } else {
                    //LOG4CXX_DEBUG( __glb_logger, ">>      ! ERROR READ FROM DEVICE [not ACK] 0x" << std::hex << repl );
                }
            } else {
                //LOG4CXX_DEBUG( __glb_logger, ">>      ! ERROR READ FROM DEVICE [no connect]" );
                //!!!!!!!!!! send ENQ
                //LOG4CXX_DEBUG( __glb_logger, ">>       SEND ENQ TO DEVICE[ get data one more time ]" );
                sendENQ(); //
                //usleep(prop->Timeout * MICRO_TO_MILLI); // подождать N*2 мс перед отправкой следующей команды
                repl = readbyte(prop->Timeout*2);
                if (__glb_connected != 1 || repl != ACK) {
                    if(__glb_connected != 1){
                        //LOG4CXX_DEBUG( __glb_logger, ">>       ! ERROR READ FROM DEVICE [ **** NO CONNECT WHEN WRITE AND SEND ENQ CMD **** ]" );
                    }
                    //tries++;
                }
            };
        } else {
            //LOG4CXX_DEBUG( __glb_logger, ">>       ! ERROR WRITE TO DEVICE [" << std::dec << result << "]" );
        }
        //LOG4CXX_DEBUG( __glb_logger, ">>        usleep [ " << std::dec << (prop->Timeout * MICRO_TO_MILLI) << " usec ] before REPEAT " );
        usleep(prop->Timeout * MICRO_TO_MILLI); // подождать N мс перед отправкой команды еще раз
        auto_increase_timeout();
    };
    return -1;
};

//--------------------------------------------------------------------------------------

int readanswertimeout(int comm) {
    
    int time_count = 30000;
    // TODO )
    if( comm >= 0 && comm < sizeof( wait_device)/ sizeof( &wait_device ) ){
        time_count = wait_device[comm];
    }
    //LOG4CXX_DEBUG( __glb_logger, "= time count to wait prepere data on device " << std::dec << time_count );
    return time_count;
}

//--------------------------------------------------------------------------------------

int readanswer(int comm,answer *ans) {
    //LOG4CXX_DEBUG( __glb_logger, "<< readanswer data for command 0x" << std::hex << comm );
    //perror("<< readanswer data for command");
    short int len, crc, tries;
    for (tries = 1; tries <= __glb_args.max_tries; tries++) {
        //LOG4CXX_DEBUG( __glb_logger, "<<       tries from MAX  : " << std::dec << tries << " / " << std::dec << __glb_args.max_tries );
        short int repl = readbyte(readanswertimeout(comm)); // подождать n мс пока устройство подготовит данные для ответа( зависит от команды )
        
        if (__glb_connected == 1) {
            if (repl == STX) {
                //LOG4CXX_DEBUG( __glb_logger, "<<      SUCCESS [STX] 0x" << std::hex << repl );
                //perror("<<      SUCCESS [STX]");
                len = readbyte();
                if (__glb_connected == 1) {
                    //LOG4CXX_DEBUG( __glb_logger, "<<      SUCCESS LEN ANSWER " << std::dec << len );
                    //perror("<<      SUCCESS LEN ANSWER");
                    if (readbytes(ans->buff, len) == len) {
                        crc = readbyte();
                        if (__glb_connected != 0) {
                            if (crc == (LRC(ans->buff, len, 0) ^ len)) {
                                //LOG4CXX_DEBUG( __glb_logger, "<<       SUCCESS LRC " << std::hex << crc );
                                //LOG4CXX_DEBUG( __glb_logger, "<<       SEND ACK TO DEVICE" );
                                //perror("<<       SEND ACK TO DEVICE");
                                sendACK();
                                //perror("<<       SEND ACK TO DEVICE");
                                usleep(prop->Timeout * MICRO_TO_MILLI); // подождать 50 мс перед отправкой следующей команды
                                ans->len = len;
                                //current_command = -1;
#ifdef DEBUG
    PrintAnswer(ans);
#endif
                                return len;
                            } else {
                                //LOG4CXX_ERROR( __glb_logger, "<<       ! ERROR READ FROM DEVICE [ LRC ERROR ]" );
                                //LOG4CXX_DEBUG( __glb_logger, "<<       SEND NAK TO DEVICE" );
                                //perror("<<       SEND NAK TO DEVICE");
                                sendNAK();
                                //perror("<<       SEND NAK TO DEVICE");
                                usleep(prop->Timeout * MICRO_TO_MILLI); // подождать 50 мс перед отправкой следующей команды
                            }
                        } else {
                            //LOG4CXX_ERROR( __glb_logger, "<<       ! ERROR READ FROM DEVICE [cant't read len " << len << " bytes ]" );
                        }
                    } else {
                        //LOG4CXX_DEBUG( __glb_logger, "<<       ! ERROR READ FROM DEVICE [first byte not STX]" );
                    }
                } else {
                    //LOG4CXX_ERROR( __glb_logger, "<<       ! ERROR READ FROM DEVICE [read len]" );
                }
            } else {
                //LOG4CXX_ERROR( __glb_logger, "<<       ! ERROR READ FROM DEVICE [first byte not STX]" );
            }
        } else {
            //LOG4CXX_DEBUG( __glb_logger, "<<       ! ERROR READ FROM DEVICE [no connect]" );
        }

        //LOG4CXX_DEBUG( __glb_logger, "<<       SEND ENQ TO DEVICE[ put data one more time ]" );
        sendENQ(); //
        //usleep(prop->Timeout * MICRO_TO_MILLI); // подождать 50 мс перед отправкой следующей команды
        repl = readbyte(prop->Timeout*2);
        if (__glb_connected != 1 || repl != ACK) {
            if(__glb_connected != 1){
                //LOG4CXX_ERROR( __glb_logger, "<<       ! ERROR READ FROM DEVICE [ **** NO CONNECT WHEN READ AND SEND ENQ CMD **** ]" );
            }
            tries++;
        }
    };
    //current_command = -1;
    return -1;
}
//--------------------------------------------------------------------------------------

int clearanswer(void) {
    short int len;
    unsigned char buf[0x100];

    sendENQ();
    len = readbytes(buf, 0x100);
    if (__glb_connected == 1 && len > 0) {
        if (buf[0] != NAK) {
            sendACK();
            return 1;
        };
    };
    return 0;
}
//--------------------------------------------------------------------------------------

const char* devname(int number) {
    const char *devn[44] = {
        "/dev/null",
        "/dev/ttyACM0",
        "/dev/ttyACM1",
        "/dev/ttyACM2",
        "/dev/ttyACM3",
        "/dev/ttyACM4",
        "/dev/ttyACM5",
        "/dev/ttyACM6",
        "/dev/ttyACM7",
        "/dev/ttyACM8",
        "/dev/ttyACM9",
        "/dev/ttyACM10",
        "/dev/ttyACM11",
        "/dev/ttyACM12",
        "/dev/ttyACM13",
        "/dev/ttyACM14",
        "/dev/ttyACM15",
        "/dev/ttyACM16",
        "/dev/ttyACM17",
        "/dev/ttyACM18",
        "/dev/ttyACM19",
        "/dev/ttyACM20",
        "/dev/ttyUSB0",
        "/dev/ttyUSB1",
        "/dev/ttyUSB2",
        "/dev/ttyUSB3",
        "/dev/ttyUSB4",
        "/dev/ttyUSB5",
        "/dev/ttyUSB6",
        "/dev/ttyUSB7",
        "/dev/ttyUSB8",
        "/dev/ttyUSB9",
        "/dev/ttyUSB10",
        "/dev/ttyS0",
        "/dev/ttyS1",
        "/dev/ttyS2",
        "/dev/ttyS3",
        "/dev/ttyS4",
        "/dev/ttys5",
        "/dev/ttyS6",
        "/dev/ttyS7",
        "/dev/ttyS8",
        "/dev/ttyS9",
        "/dev/ttyS10"
    };
    if (number > 0 && number < 44) return strdup(devn[number]);
    return strdup(devn[0]);
}
//--------------------------------------------------------------------------------------
