/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   KKTAPI.cpp
 * Author: semen
 * 
 * Created on 13 марта 2018 г., 10:42
 */

#include <map>
#include <string>
#include <sstream>

#include "KKTAPI.h"

ApiMethods KKTAPI::_api_methods;

KKTAPI::KKTAPI() 
{}

KKTAPI::KKTAPI(const KKTAPI& orig) 
{}

KKTAPI::~KKTAPI() 
{}

std::string KKTAPI::json_head(fr_func* fn)
{
    std::stringstream ss;
    ss << "{";
    ss << "\"status\":\"" << (fn->prop->ResultCode==0?"success":"error") << "\",";
    ss << "\"cmd\":\"" << "" << "\",";
    ss << "\"ans\":\"" << "" << "\",";
    ss << "\"result\":{"
            << "\"value\":\"" << fn->prop->ResultCode << "\","
            << "\"description\":\"" << fn->prop->ResultCodeDescription << "\""
       << "},";
    ss << "\"data\":{";
    return ss.str();
}

std::string KKTAPI::json_foot(fr_func* fn)
{
    std::stringstream ss;
    ss << "}";
    ss << "}";
    return ss.str();
}

std::string KKTAPI::api_get_short_status(fr_func* fn, fr_prop* prop, std::map<const char *, const char *> params)
{
    int result = fn->GetShortStatus();
    std::stringstream ss;

    return ss.str();
}

std::string KKTAPI::api_get_ecr_status(fr_func* fn, fr_prop* prop, std::map<const char *, const char *> params)
{
    int result = fn->GetECRStatus();
    std::stringstream ss;

    return ss.str();
}

std::string KKTAPI::api_get_fn_status(fr_func* fn, fr_prop* prop, std::map<const char *, const char *> params)
{

    int result = fn->GetFNStatus();

    std::stringstream ss;
            
        ss << "\"fn_life_state\":{";
        ss << "\"title\":\"Состояние фазы жизни\","
            << "\"value\":\"" << prop->FNLifeState << "\","
            << "\"description\":\""<< "" <<"\""
           << "},";

        ss << "\"fn_current_document\":{";
        ss << "\"title\":\"Текущий документ\","
            << "\"value\":\"" << prop->FNCurrentDocument << "\","
            << "\"description\":\"" << get_currdocdesc_message(prop->FNCurrentDocument) << "\""
           << "},";

        ss << "\"fn_document_data\":{";
        ss << "\"title\":\"Данные документа\","
            << "\"value\":\""  << prop->FNDocumentData << "\","
            << "\"description\":\"\""
           << "},";

        ss << "\"fn_sassion_state\":{";
        ss << "\"title\":\"Состояние смены\","
            << "\"value\":\"" << prop->FNSessionState << "\","
            << "\"description\":\"\""
           << "},";

        ss << "\"fn_sassion_state\":{";
        ss << "\"title\":\"Флаги предупреждения\","
           << "\"value\":\"" << prop->FNWarningFlags << "\","
           << "\"description\":\"\","
           << "\"values\":"
                << "["
                    << "{"
                        << ""
                    << "},"
                << "]"
           << "},";

        // Дата и время
        //fr.Date
        //fr.Time
        ss << "\"date\":{";
        ss << "\"title\":\"Дата\","
            << "\"value\":\"\","
            << "\"description\":\"\""
           << "},";
        ss << "\"time\":{";
        ss << "\"title\":\"Время\","
            << "\"value\":\"\","
            << "\"description\":\"\""
           << "},";
        // Номер Фискального Накапителя
        //fr.SerialNumber
        ss << "\"serial_number\":{";
        ss << "\"title\":\"Номер фискального накапителя\","
            << "\"value\":\"" << prop->SerialNumber << "\","
            << "\"description\":\"\""
           << "},";
        // Номер последнего Фискального Документа
        //fr.DocumentNumber
        ss << "\"document_number\":{";
        ss << "\"title\":\"Номер последнего фискального документа\","
            << "\"value\":\"" << prop->DocumentNumber << "\","
            << "\"description\":\"\""
           << "}";

    return ss.str();
}

std::string KKTAPI::api_clear_fn_status(fr_func* fn, fr_prop* prop, std::map<const char *, const char *> params)
{
    int result = 0;//fn->ClearFNStatus();
    std::stringstream ss;

    
    
    return ss.str();
}
 
void KKTAPI::init_map_methods() 
{
    KKTAPI::_api_methods["get-short-status"] = KKTAPI::api_get_short_status;
    KKTAPI::_api_methods["get-ecr-status"]   = KKTAPI::api_get_ecr_status;
    KKTAPI::_api_methods["get-fn-status"]    = KKTAPI::api_get_fn_status;
    KKTAPI::_api_methods["clear-fn-status"]  = KKTAPI::api_clear_fn_status;
}

bool KKTAPI::has_api_method( const char *method ) 
{
    if( KKTAPI::_api_methods.find(method) != KKTAPI::_api_methods.end() ){
        return true;
    }
    return false;
}

std::string KKTAPI::api_method( const char* method, fr_func* fn, fr_prop* prop, const char* str_params)
{
    std::map<const char *, const char *> params;
    std::map<const char *, std::string (*)(fr_func*, fr_prop*, std::map<const char *, const char *>)>::iterator it = KKTAPI::_api_methods.find(method);
    if(it != KKTAPI::_api_methods.end() ){
        std::stringstream ss;
        std::string s = it->second(fn,prop,params);
        ss << KKTAPI::json_head(fn);
        ss << s;
        ss << KKTAPI::json_foot(fn);
        return ss.str();
    }
    
    std::stringstream ss;
    ss << "{"
       << "\"status\":\"" << "error" << "\","
       << "\"message\":\" api " << method << " - not found!" << "\""
       << "}";
    return ss.str();
}

ApiMethods KKTAPI::get_api_methods(){
    return KKTAPI::_api_methods;
}