<?php
class Printer
{

    public $receiptPath;
    public $cmd;

    protected $_filename;

    protected $_vendor;
    protected $_model;
    protected $_maxRowChars;
    protected $_raster;

    public function __construct($cmd,$receiptPath)
    {
        $this->cmd = $cmd;
        $this->receiptPath = $receiptPath;
        $this->_detect();
    }

    protected function _detect()
    {
        $this->_vendor      = 'CUSTOM';
        $this->_model       = '';
        $this->_maxRowChars = 54;
        $this->_raster      = false;
    }

    public function getTemplatePrefix()
    {
        return strtolower($this->_vendor);
    }

    /*
    public function prepareReceipt($receipt,$data=array())
    {

    }
    */

    public function writeReceipt($receipt,$token=null,$cutCompletely=true)
    {
        $replaces = ["\n"=>'',"\r"=>''];
        $cutChar = $cutCompletely?(chr(0x0A).chr(0x0c)):(chr(0x0A).chr(0x0c));
        if( !is_null($token) && strlen($token) > 0 ) {
            $barcode = $this->generateReceiptBarcode($token);//генерируем баркод из токена
        }
        /*echo nl2br(strtr($receipt,[' '=>'&nbsp;']) . (!empty($barcode)?$barcode:'') . $cutChar );
        echo '<br/><br/><br/><br/><br/><br/><br/><br/>';*/

        /*$parts = explode("\n",$receipt);
        foreach( $parts as $key=>$part )
        {
            //header('Content-Type: text/html; charset=utf-8', true);
            //$str = trim( strtr($part,$replaces), "\t\n ");
            //$str = trim( $part, "\t\n ");
            $str = $part."\n";
            $parts[$key] = $this->prepareDotted($str);
            //$parts[$key] = $str;
        }
        $receipt = $this->prepareSpecialSymbols(implode('',$parts));*/
        $receipt = $this->prepareSpecialSymbols($receipt);

        /*echo nl2br(strtr($receipt,[' '=>'&nbsp;']) . (!empty($barcode)?$barcode:'') . $cutChar );
        echo '<br/><br/><br/><br/><br/><br/><br/><br/>';*/

        $filename = date('d_m_Y_H_i_s').'_'.strtr(microtime(),' ','_');//дата в имени штрих кода
        //print_r($receipt);

        $receipt = iconv('utf8', 'cp866//TRANSLIT', $receipt);//конвертируем чек в формат печати
        if( !$this->_raster ) {
        }

        /*echo '<hr>';
        print_r($receipt);

        die('test');*/
        if(!empty($barcode)) {
            $receipt .= $barcode;//добавляем штрих код и отрезание в файл чека
        }
        //$receipt .= $cutChar;
        $path     = $this->receiptPath . $filename;//получаем полное имя файла чека

        file_put_contents($path,$receipt);
        usleep(50); //ну что бы уж точно записал))
        $this->_filename = $filename;
        return $this;
    }

    public function prepareDotted($str)
    {
        if( ( $posS = mb_strpos($str, ' ...',0,'utf-8') ) !== false
            && ( $posE = mb_stripos( $str, '... ',0,'utf-8') ) !== false
        ) {
            //echo "<pre>";
            $strS = mb_substr($str, 0, $posS,'utf-8');
            //print_r($strS);echo "\n";
            $strE = mb_substr($str, $posE+strlen('... '), mb_strlen($str,'utf-8'),'utf-8');
            //print_r($strE);echo "\n";
            $_strS = $this->clearSpecialSymbols($strS);
            //print_r($_strS);echo "\n";
            $_strE = $this->clearSpecialSymbols($strE);
            //print_r($_strE);echo "\n";

            $len = mb_strlen($_strS.$_strE,'utf-8')+2;
            //echo '______________________$len';var_dump($len);echo "\n";
            $dotedCount = $this->_maxRowChars - $len;
            //echo '_______________$dotedCount';var_dump($dotedCount);echo "\n";

            if($dotedCount <= 0){
                $str = $strS.' '.str_repeat('.', $this->_maxRowChars - mb_strlen($_strS,'utf-8')-1)."{0x0A}";
                $str .= str_repeat('.', $this->_maxRowChars - mb_strlen($_strE,'utf-8')-1).' '.$strE;
            } else {
                $str = $strS.' '.str_repeat('.', $dotedCount).' '.$strE;
            }
            //print_r($str);echo "\n";
            //echo '_______________$dotedCount';mb_strlen($str,'utf-8');echo "\n";
            //echo "</pre>";

        }
        return $str;
    }

    public function printReceipt()
    {
        if( empty( $this->_filename ) ){
            return false;
        }
        $result = array();

        system($this->cmd . ' ' .$this->receiptPath.$this->_filename, $result);//печатаем чек из файла

        return $result;
    }

    public function generateReceiptBarcode($uniqueToken)
    {
        return chr(0x1D) . chr(0x6B) . chr(73) . chr(24) . chr(0x7b) . chr(0x42) . $uniqueToken;
    }

    /**
     * Подготавливает спец-символы в шаблоне чека и превращает их в байты
     * @param	string	$receipt	Description
     * @return	string				Description
     */
    public function clearSpecialSymbols($receipt)
    {
        $receipt = str_replace("\n", "", $receipt);
        preg_match_all("/\{([^\}]+)\}/iU", $receipt, $matches);
        $symbols = array_unique($matches[1]);
        foreach($symbols as $symbol) {
            $receipt = preg_replace('/\{' . $symbol. '\}/', '', $receipt);
        }
        return $receipt;
    }

    /**
     * Подготавлаивает строку для печати
     * @param	string	$str
     * @return	string
     */
    static public function prepareStringToPrint($str)
    {
        $parts = explode("\n",$str);
        $replaces = ["\n"=>'',"\r"=>''];
        $str = '';
        foreach( $parts as $key=>$part ) {
            //header('Content-Type: text/html; charset=utf-8', true);
            $str .= trim(strtr($part, $replaces), "\t\n ").'{0x0A}';
        }
        return $str;
    }

    /**
     * Подготавливает спец-символы в шаблоне чека и превращает их в байты
     * @param	string	$receipt
     * @return	string
     */
    public function prepareSpecialSymbols($receipt) {
        //$receipt = str_replace("\n", "", $receipt);
        preg_match_all("/\{([^\}]+)\}/iU", $receipt, $matches);
        print_r($matches);
        $symbols = array_unique($matches[1]);
        foreach($symbols as $symbol) {
            $receipt = preg_replace('/\{' . $symbol. '\}/', chr($this->checkCharCode($symbol)), $receipt);
        }
        return $receipt;
    }
    /**
     * Возвращает байт для спец-символа в шаблоне чека
     * @param	string	$symbol	Description
     * @return	string			Байт вида 0x00
     */
    public function checkCharCode($symbol) {
        switch($symbol) {
            case 'ESC':
                return 0x1B;
            case 'LF':
                return 0x0A;
            case 'FF':
                return 0x0C;
            case 'NUL':
                return 0x00;
            case 'HT':
                return 0x09;
            case 'GS':
                return 0x1D;
            default:
                if (strpos($symbol, 'x') !== false) {
                    return $symbol;
                }
                if (strpos($symbol, "'") !== false) {
                    $char = str_replace("'", "", $symbol);
                    return ord($char);
                }
                return ('0x' . (string)dechex((int)$symbol));
        }

        return $symbol;
    }
}
