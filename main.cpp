#include <iostream>
#include <cstdlib>
#include <string.h>
#include <stdio.h>
#include <getopt.h>

#include <ctime>

#include <stdint.h>
#include <sstream>
#include <algorithm>
#include <vector> 
#include <bitset>
#include <fcntl.h>
#include <termios.h>

/*
#include <log4cxx/logger.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/exception.h>

using namespace log4cxx;
using namespace log4cxx::helpers;

extern const LoggerPtr __glb_logger(Logger::getLogger("shtrih_m"));
extern const LoggerPtr __glb_logger_result(Logger::getLogger("shtrih_m.results"));
 */


#include "interface.h"
#include "typedefs.h"
#include "drvfr.h"
#include "options.h"
#include "conn.h"
#include "KKTAPI.h"

#ifndef VERSION_MAJOR
#define VERSION_MAJOR 0
#endif

#ifndef VERSION_MINOR
#define VERSION_MINOR 0
#endif

#ifndef VERSION_MAINTENANCE
#define VERSION_MAINTENANCE 2
#endif

#ifndef VERSION_TYPE
#define VERSION_TYPE "r"
#endif

#ifndef DEVELOP_EMAIL
#define DEVELOP_EMAIL "it@vsekioski.ru"
#endif




#ifndef DEBUG
#define DEBUG 0
#endif

#if DEBUG > 0
#define D(x) x
#define SDEBUG(x) do { std::cerr << x; } while (0)
#else
#define D(x)
#define SDEBUG(x) do {} while (0)
#endif


#ifndef DEBUG_LEVEL
#define DEBUG_LEVEL 0
#endif

        
using namespace std;

void usage(char *name) {
    std::cout << "usage " << name << std::endl;
    std::cout << "\t-h[--help] this message" << std::endl;
    std::cout << "\t-v[--version] version" << std::endl;
    std::cout << "\t-d[--debug] tune on debug value id debug level(yet not released)" << std::endl;
    std::cout << "\t-D[--detect] auto detect fiskal(yet not released)" << std::endl;
    std::cout << "\t-a[--autoz] apply auto close shift if shift is timeout, default value is 0 - no autoZ, 1 - print report with cleaning to the bufer, 2 - print report with cleaning in the check" << std::endl;
    std::cout << "\t-e[--execute] execute command: " << std::endl
            << "\t\tz - repotr with cleaning" << std::endl
            << "\t\tzb - report with cleaning to the bufer" << std::endl
            << "\t\tx - repotr without cleaning" << std::endl
            << "\t\ts - short status" << std::endl
            << "\t\tS - full status" << std::endl
            << "\t\tm - device metrics" << std::endl
            << "\t\tf - shift status" << std::endl
            << "\t\tс - cut check" << std::endl
            << "\t\timg=[absolute path to bmp image in 24 or 32 bits] - print image on printer" << std::endl
            << "\t\tTest - start a test run" << std::endl
            << "\t\tEndTest - end a test run" << std::endl
            << "\t\tsync-time - synchronize date and time" << std::endl
            << "\t\tfield-value=[table num]-[row num]-[field num] - get field table value" << std::endl;
    
            KKTAPI::init_map_methods();
            std::cout << "\t\tAPI methods;)" << std::endl;
            ApiMethods methods = KKTAPI::get_api_methods();
            for ( ApiMethods::iterator it = methods.begin(); it != methods.end(); it++ ) {
                std::cout << "\t\t" << it->first << std::endl;
            }
            
    std::cout << "\t-с[--cut-off] cut receipt" << std::endl;
    std::cout << "\t-b[--baudrate] values: 2400,4800,9600,19200,38400,57600,115200,230400,460800,921600 default value is 115200" << std::endl;
    std::cout << "\t-f[--flow] values: 3,2,1,0 default value is 0" << std::endl;
    std::cout << "\t-p[--port] device port, default value is /dev/ttyACM0 " << std::endl;
    std::cout << "\t-P[--password] password for operator, default value is 30 " << std::endl;
    std::cout << "\t--password-sc password for service center, default value is 30 " << std::endl;
    std::cout << "\t--password-ti password for tax inspector, default value is 30 " << std::endl;
    std::cout << "\t--password-adm password for system admiistrator, default value is 30 " << std::endl;
    std::cout << "\t--auto-timeout use auto increase for timeout, default value is 1 " << std::endl;
    std::cout << "\t--step-timeout step for auto increase timeout, default value is 5 msec" << std::endl;
    std::cout << "\t--max-timeout max value to increase timeout, default value is 100 msec" << std::endl;
    std::cout << "\t-t[--timeout] timeout for send of byte, default value is 50 msec " << std::endl;
    std::cout << "\t--connect-tries max tries to send ENQ(init connect to device/init send com), default value is 15" << std::endl;
    std::cout << "\t--execute-tries max tries to execute cmd, default value is 3" << std::endl;
    std::cout << "\t--resend-tries max tries to recend cmd before re execute(reconnect), default value is 10" << std::endl;
    std::cout << "\t--rw-tries max tries to read ans or write cmd to device timeout, default value is 10" << std::endl << std::endl;
    std::cout << "\t--out-receipt o - out, i - retractor(int) if not set used KKT settings" << std::endl;
    std::cout << "\t--out-report  o - out, i - retractor(int) if not set used KKT settings" << std::endl;
    std::cout << "Also use environments: " << std::endl
            << "\tFR_PASSWORD=[password for operator] password for operator, default value is 30" << std::endl
            << "\tFR_PASSWORD_TI=[password for tax inspector] default value is 30" << std::endl
            << "\tFR_PASSWORD_SC=[password for service center] default value is 30" << std::endl
            << "\tFR_AUTOZ=[1|0] apply auto close shift if shift is timeout, default value is 0" << std::endl;
    
    std::cout << std::endl << "Codes in receipt: " << std::endl
            << "\t{ESC}O - open shift" << std::endl
            << "\t{ESC}Op - open check" << std::endl
            << "\t{ESC}Cl - close check" << std::endl
            << "\t{ESC}Qr [int - version] [int - mask] [int - dot size] [int - error correct level] [int - qr align] [str - string to code] - print QR code" << std::endl
            << "\t{ESC}R [string - title of sale without spaces] [int - num of department] [float - amount(delimeter is .)] - sale cash. this cmd open check, do sale, close check(print fiscal part of receipt)" << std::endl
            << "\t{ESC}S [int - type of cute: 0 - half cute, 1 - full cute. not required, default is 1] - cut receipt " << std::endl
            << "\t{ESC}Sl [string - title of sale without spaces] [int - num of department] [float - amount(delimeter is .)] - sale without open and close receipt" << std::endl
            << "\t{ESC}Sb - check sub total" << std::endl
            << "\t{ESC}T [string - title of sale without spaces] [int - num of department] [float - amount(delimeter is .)] - same as R but echo link for OFD" << std::endl
            << "\t{ESC}Tx [int - number of tax from sequense [1,4]] [int - 1 - use this tax for next sale, 0 - don't use this tax] - set number for tax value. After tuned on is used for all sales until it is turned off" << std::endl
            << "\t{ESC}Z - print report with close shift and clear" << std::endl
            << "\t{ESC}X - print report without close shift and without clear" << std::endl
            << "\t{ESC}F - feed document" << std::endl
            << "\t{ESC}N - empty sale with tax1 is off" << std::endl
            << "\t{ESC}H[string for code] - print code128 barcode" << std::endl
            << "\t{ESC}Im [int - ] [string - absolute path to bmp (24 or 32 bits) image] - prin image" << std::endl
            << "\tB[string to print] - print header font1" << std::endl
            << "\tb[string to print] - print simple string" << std::endl;

    std::cout << std::endl << name << " --port=/dev/ttyS0 --baudrate=9600 --flow=1 --password=30 < [receipt template file]" << std::endl;
}

void version(char *name) {
    std::cout << name << " version " << VERSION_MAJOR << "." << VERSION_MAJOR << "." << VERSION_MAINTENANCE;
#ifdef VERSION_TYPE
    stringstream ss;
    ss << VERSION_TYPE;
    if (ss.str().length() > 0) {
        std::cout << "-" << ss.str();
    }
#endif
    std::cout << std::endl << "STC-soft <" << DEVELOP_EMAIL << ">" << std::endl;
}

void prepare_arguments(int argc, char **argv) {
    int rez = 0;
    int longIndex = 0;

    static const char *optString = "a:b:cd:De:f:p:P:t:vh?";
    static const struct option longOpts[] = {
        { "autoz", required_argument, NULL, 'a'},
        { "baudrate", required_argument, NULL, 'b'},
        { "cut-off", no_argument, NULL, 'c'},
        { "debug", required_argument, NULL, 'd'},
        { "detect", required_argument, NULL, 'D'},
        { "execute", required_argument, NULL, 'e'},
        { "flow", required_argument, NULL, 'f'},
        { "port", required_argument, NULL, 'p'},
        { "password", required_argument, NULL, 'P'},
        { "password-sc", required_argument, NULL, -1},
        { "password-ti", required_argument, NULL, -2},
        { "password-adm", required_argument, NULL, -3},
        { "auto-timeout", required_argument, NULL, -4},
        { "step-timeout", required_argument, NULL, -5},
        { "max-timeout", required_argument, NULL, -6},
        { "connect-tries", required_argument, NULL, -7},
        { "execute-tries", required_argument, NULL, -8},
        { "resend-tries", required_argument, NULL, -9},
        { "rw-tries", required_argument, NULL, -10},
        { "out-receipt", required_argument, NULL, -11},
        { "out-report", required_argument, NULL, -12},
        { "timeout", required_argument, NULL, 't'},
        { "version", no_argument, NULL, 'v'},
        { "help", no_argument, NULL, 'h'},
        { NULL, no_argument, NULL, 0}
    };

    __glb_args.debug = 0;
    __glb_args.debug_level = 0;

    __glb_args.port = (char *)DEFAULT_PORT;
    __glb_args.baudrate = DEFAULT_BAUDRATE;
    __glb_args.flow = 0;
    
    __glb_args.password = (char *)DEFAULT_PASSWORD;
    __glb_args.password_sc = (char *)DEFAULT_PASSWORD;
    __glb_args.password_ti = (char *)DEFAULT_PASSWORD;
    
    __glb_args.timeout = DEFAULT_TIMEOUT;
    __glb_args.use_auto_timeout = USE_AUTO_TIMEOUT;
    __glb_args.step_auto_timeout = STEP_AUTO_TIMEOUT;
    __glb_args.max_auto_timeout = MAX_AUTO_TIMEOUT;

    __glb_args.max_connect_tries = MAX_CONNECT_TRIES;
    __glb_args.max_execute_tries = MAX_EXECUTE_TRIES;
    __glb_args.max_resend_tries = MAX_RESEND_TRIES;
    __glb_args.max_tries = MAX_TRIES;
    
    __glb_args.autoz = 0;
    __glb_args.cut_off = 0;
    __glb_args.cmd = (char *)"";

    if (DEBUG) {
        __glb_args.debug = DEBUG;
    }
    if (DEBUG_LEVEL) {
        __glb_args.debug_level = DEBUG_LEVEL;
    }
    if (std::getenv("FR_PASSWORD") != 0) {
        sscanf(std::getenv("FR_PASSWORD"), "%s", __glb_args.password);
    }
    if (std::getenv("FR_PASSWORD_TI") != 0) {
        sscanf(std::getenv("FR_PASSWORD_TI"), "%s", __glb_args.password_ti);
    }
    if (std::getenv("FR_PASSWORD_SC") != 0) {
        sscanf(std::getenv("FR_PASSWORD_SC"), "%s", __glb_args.password_sc);
    }
    if (std::getenv("FR_PASSWORD_ADMIN") != 0) {
        sscanf(std::getenv("FR_PASSWORD_ADMIN"), "%s", __glb_args.password_admin);
    }
    if (std::getenv("FR_AUTOZ") != 0) {
        sscanf(std::getenv("FR_AUTOZ"), "%d", &__glb_args.autoz);
    }

    //	opterr=0;
    while ((rez = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1) {
        switch (rez) {
            case 'a': // auto shift
                //printf("found argument \"autoz[a] = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.autoz);
                break;
            case 'b': // 
                //printf("found argument \"baudrate[b] = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.baudrate);
                break;
            case 'c': // 
                //printf("found argument \"cut-off[c] = %s\".\n", optarg);
                __glb_args.cut_off = 1;
                break;
            case 'd': // debug, value as debug-level value
                //printf("found argument \"debug[d] = %s\".\n", optarg);
                __glb_args.debug = 1;
                sscanf(optarg, "%d", &__glb_args.debug_level);
                break;
            case 'e': // 
                //printf("found argument \"execute[e] = %s\".\n", optarg);
                __glb_args.cmd = optarg;
                break;
            case 'f': // 
                //printf("found argument \"flow[f] = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.flow);
                break;
            case 'p': // 
                //printf("found argument \"port[p] = %s\".\n", optarg);
                __glb_args.port = optarg;
                break;
            case 'P': // 
                //printf("found argument \"password[P] = %s\".\n", optarg);
                __glb_args.password = optarg;
                break;
            case -1:
                //printf("found argument \"password-sc = %s\".\n", optarg);
                __glb_args.password_sc = optarg;
                break;
            case -2:
                //printf("found argument \"password-ti = %s\".\n", optarg);
                __glb_args.password_ti = optarg;
                break;
            case -3:
                //printf("found argument \"password-adm = %s\".\n", optarg);
                __glb_args.password_admin = optarg;
                break;
            case -4:
                //printf("found argument \"auto-timeout = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.use_auto_timeout);
                break;
            case -5:
                //printf("found argument \"step-timeout = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.step_auto_timeout);
                break;
            case -6:
                //printf("found argument \"max-timeout = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.max_auto_timeout);
                break;
            case -7:
                //printf("found argument \"connect-tries = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.max_connect_tries);
                break;
            case -8:
                //printf("found argument \"execute-tries = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.max_execute_tries);
                break;
            case -9:
                //printf("found argument \"resend-tries = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.max_resend_tries);
                break;
            case -10:
                //printf("found argument \"max-tries = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.max_tries);
                break;
            case -11:
                //printf("found argument \"out-receipt = %с\".\n", optarg);
                sscanf(optarg, "%c", &__glb_args.out_receipt);
                break;
            case -12:
                //printf("found argument \"out-report = %с\".\n", optarg);
                sscanf(optarg, "%c", &__glb_args.out_report);
                break;
            case 't': // 
                //printf("found argument \"timeout[t] = %s\".\n", optarg);
                sscanf(optarg, "%d", &__glb_args.timeout);
                break;
            case 'v':
                version(argv[0]);
                exit(EXIT_SUCCESS);
                break;
            case 'h':
            case '?':
            default:
                usage(argv[0]);
                exit(EXIT_SUCCESS);
                break;
        };
    };
#if DEBUG > 0
    std::stringstream log_ss;
    log_ss << "    prepared args " << std::endl;
    log_ss << "    __glb_args.debug = " << __glb_args.debug << std::endl;
    log_ss << "    __glb_args.debug_level = " << __glb_args.debug_level << std::endl;
    log_ss << "    __glb_args.port = " << __glb_args.port << std::endl;
    log_ss << "    __glb_args.baudrate = " << __glb_args.baudrate << std::endl;
    log_ss << "    __glb_args.flow = " << __glb_args.flow << std::endl;
    log_ss << "    __glb_args.password = " << __glb_args.password << std::endl;
    log_ss << "    __glb_args.password_ti = " << __glb_args.password_ti << std::endl;
    log_ss << "    __glb_args.password_sc = " << __glb_args.password_sc << std::endl;
    log_ss << "    __glb_args.password_admin = " << __glb_args.password_admin << std::endl;
    
    log_ss << "    __glb_args.use_auto_timeout = " << __glb_args.use_auto_timeout << std::endl;
    log_ss << "    __glb_args.step_auto_timeout = " << __glb_args.step_auto_timeout << std::endl;
    log_ss << "    __glb_args.max_auto_timeout = " << __glb_args.max_auto_timeout << std::endl;

    log_ss << "    __glb_args.max_connect_tries = " << __glb_args.max_connect_tries << std::endl;
    log_ss << "    __glb_args.max_execute_tries = " << __glb_args.max_execute_tries << std::endl;
    log_ss << "    __glb_args.max_resend_tries = " << __glb_args.max_resend_tries << std::endl;
    log_ss << "    __glb_args.max_tries = " << __glb_args.max_tries << std::endl;

    log_ss << "    __glb_args.out_receipt = " << __glb_args.out_receipt << std::endl;
    log_ss << "    __glb_args.out_report = " << __glb_args.out_report << std::endl;
    
    log_ss << "    __glb_args.timeout = " << __glb_args.timeout << std::endl;
    log_ss << "    __glb_args.autoz = " << __glb_args.autoz << std::endl;
    log_ss << "    __glb_args.cut_off = " << __glb_args.cut_off << std::endl;
    log_ss << "    __glb_args.cmd = " << __glb_args.cmd << std::endl;
    std::cout << log_ss.str();
    ////LOG4CXX_DEBUG( __glb_logger, log_ss.str() );
#endif
}

void displayResultGetShortStatus(fr_func* fr, fr_prop* prop)
{    
    printf("\nGetShortStatus ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    printf("%d - %s\n", prop->OperatorNumber, "Operator number");
    printf("%u - %s\n", prop->ECRFlags, "ECR Flags");
    printf("%d - %s\n", prop->ECRMode, "ECR Mode");
    printf("%s - %s\n", prop->ECRModeDescription, "ECR Mode Description");
    printf("%d - %s\n", prop->ECRSubmode, "ECR Submode");
    printf("%s - %s\n", prop->ECRSubmodeDescription, "ECR Submode Description");
    printf("%u - %s\n", prop->ECRCountOperationInReceiptHight, "ECR Count Operation In Receipt Hight");
    printf("%u - %s\n", prop->ECRCountOperationInReceiptLow, "ECR Count Operation In Receipt Low");
    printf("%d - %s\n", prop->ECRVoltageOfReservBattary, "ECR Voltage Of Reserv Battary");
    printf("%d - %s\n", prop->ECRVoltageOfSource, "ECR Voltage Of Source");
    printf("%u - %s\n", prop->ECRErrorCodeFP2, "ECR Error Code FP 2");
    printf("%u - %s\n", prop->ECRErrorCodeEKLZ2, "ECR Error Code EKLZ 2");
    printf("%u - %s\n\n", prop->ECRLastPrintResult, "ECR Last Print Result");
}

void displayResultGetECRStatus(fr_func* fr, fr_prop* prop)
{   
    printf("\nGetECRStatus ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    printf("%d - %s\n", prop->ResultCode, "Result code");
    printf("%d - %s\n", prop->OperatorNumber, "Operator number");

    printf("%s - %s\n", prop->FMSoftVersion, "FM Soft version");
    printf("%d - %s\n", prop->FMBuild, "FM Build");

    printf("%s - %s\n", prop->ECRSoftVersion, "ECR Soft version");
    printf("%d - %s\n", prop->ECRBuild, "ECR Build");

    printf("%d - %s\n", prop->ECRMode, "ECR Mode");
    printf("%s - %s\n", prop->ECRModeDescription, "ECR Mode Description");

    printf("%d - %s\n", prop->PortNumber, "Port number");
    printf("%s - %s\n", prop->SerialNumber, "Serial number");
    printf("%s - %s\n", prop->INN, "INN");
    /*
    std::cout << std::dec << prop->OperatorNumber << " - Operator number" << std::endl;
    std::cout << prop->FMSoftVersion[0] << " - FM Soft version" << std::endl;
    std::cout << prop->PortNumber << " - Port number" << std::endl;
    std::cout << std::dec << prop->SerialNumber << " - Serial number" << std::endl;
    std::cout << std::dec << prop->INN << " - INN" << std::endl;
    */
}

void displayResultGetFNStatus(fr_func* fr, fr_prop* prop)
{   
    printf("\nGetFNStatus ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    printf("%d - %s\n", prop->ResultCode, "Result code");
    printf("%d - %s\n", prop->OperatorNumber, "Operator number");

    // Состояние фазы жизни
    printf("%u - %s\n", prop->FNLifeState, "FN Phase of life state");

    // Текущий документ
    printf("%u - %s\n", prop->FNCurrentDocument, "FN Curren document");
    // Данные документа
    printf("%u - %s\n", prop->FNDocumentData, "FN Document data");
    // Состояние смены
    printf("%d - %s\n", prop->ShiftStatus, "FN Shift status");
    printf("%u - %s\n", prop->FNSessionState, "FN Session state");

    // Флаги предупреждения
    printf("%u - %s\n", prop->FNWarningFlags, "FN Warning flags");

    // Номер последнего ФД
    printf("%d - %s\n", prop->DocumentNumber, "FN Document number");
}
void displayResultGetDeviceMetrics(fr_func* fr, fr_prop* prop)
{   
    std::cout << "\nGetDeviceMetrics ---> " << prop->ResultCodeDescription << "[code " << prop->ResultCode << "]\n";
    std::cout << "[" << prop->UMajorType << "] - MajorType" << std::endl;
    std::cout << "[" << prop->UMinorType << "] - MinorType" << std::endl;
    std::cout << "[" << prop->UMajorProtocolVersion << "] - MajorProtocolVersion" << std::endl;
    std::cout << "[" << prop->UMinorProtocolVersion << "] - MinorProtocolVersion" << std::endl;
    std::cout << "[" << prop->UModel << "] - Model" << std::endl;
    std::cout << "[" << prop->UCodePage << "] - CodePage" << std::endl;
    std::cout << "[" << prop->UDescription << "] Description" << std::endl;
}


int storePrintReceiptOutParam(fr_func* fr, fr_prop* prop,unsigned char &status)
{
    status = 'e';
    prop->ValueOfFieldInteger;
    int result = fr->GetPrintReceiptToOut();
    printf("GetPrintReceiptToOut ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    if( !result ){
        status == 1?'i':'o';
    }
    return result;
}

int storePrintReportOutParam(fr_func* fr, fr_prop* prop,unsigned char &status)
{
    status = 'e';
    int result = fr->GetPrintReportToOut();
    printf("GetPrintReportToOut ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    if( !result  ){
        status = prop->ValueOfFieldInteger == 1?'i':'o';
    }
    return result;
}

int storagePrintOutParams(fr_func* fr, fr_prop* prop)
{
    printf("storagePrintOutParams::\n");
    int result = storePrintReceiptOutParam(fr, prop,__glb_args.out_receipt_saved);
    result = storePrintReportOutParam(fr, prop,__glb_args.out_report_saved);
    return result;
}
int setPrintReceiptOutParam(fr_func* fr, fr_prop* prop,int out_receipt)
{
    int result = 0;
    if( out_receipt >= 0 ) {
        prop->ValueOfFieldInteger = out_receipt > 0 ? 1 : 0;
        result = fr->SetPrintReceiptToOut();
        printf("SetPrintReceiptToOut ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    }
    return result;
}
int setPrintReportOutParam(fr_func* fr, fr_prop* prop,int out_report)
{
    int result = 0;
    if( out_report >= 0 ) {
        prop->ValueOfFieldInteger = out_report > 0 ? 1 : 0;
        result = fr->SetPrintReportToOut();
        printf("SetPrintReportToOut ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    }
    return result;
}
int setPrintOutParams(fr_func* fr, fr_prop* prop,int out_receipt, int out_report)
{
    printf("setPrintOutParams::\n");
    int result = setPrintReceiptOutParam(fr,prop,out_receipt);
    result = setPrintReportOutParam(fr,prop,out_report);
    return result;
}
int setPrintReceiptOutParam(fr_func* fr, fr_prop* prop,unsigned char c_out_receipt)
{
    int out_receipt = -1;
    if( c_out_receipt == 'i' ){
        out_receipt = 1;
    } else if( c_out_receipt == 'o' ) {
        out_receipt = 0;
    }
    return setPrintReceiptOutParam(fr,prop,out_receipt);
}
int setPrintReportOutParam(fr_func* fr, fr_prop* prop,unsigned char c_out_report)
{
    int out_report = -1;
    if( c_out_report == 'i' ){
        out_report = 1;
    } else if( c_out_report == 'o' ) {
        out_report = 0;
    }
    return setPrintReportOutParam(fr,prop,out_report);
}
int setPrintOutParams(fr_func* fr, fr_prop* prop, unsigned char c_out_receipt, unsigned char c_out_report)
{
    int result = setPrintReceiptOutParam(fr, prop, c_out_receipt);
    result = setPrintReportOutParam(fr, prop, c_out_report);
    return result;
}

int setPrintOutParams(fr_func* fr, fr_prop* prop)
{
    printf("setPrintOutParams::\n");
    return setPrintOutParams(fr,prop,__glb_args.out_receipt,__glb_args.out_report);
}

int unsetPrintOutParams(fr_func* fr, fr_prop* prop)
{
    printf("unsetPrintOutParams::\n");
    if(__glb_args.out_receipt!='e'||__glb_args.out_report!='e'){ // если были поменяны в параметрах
        return setPrintOutParams(fr,prop,__glb_args.out_receipt_saved,__glb_args.out_report_saved);
    }
    return 0;
}


int AutoPrintReportWithCleaning(fr_func* fr, fr_prop* prop, unsigned int autoz)
{
    int result = 0;
    // закрываем смену если стоит флаг автозакрытия смены и 
    if( autoz >= 0 ) { // если стоит автоснятие смены
        printf("     !!! AUTO Print Repor tWith Cleaning !!!    \n");
        if( autoz == 1 ){
            // сохраняем текущий флаг
            unsigned char status;
            storePrintReportOutParam(fr,prop,status);
            if( status != 'i' ) {//уже установлен параметр
                // установить флаг печати отчета в ретрактор(сбрасывать во внутрь)
                result = setPrintReportOutParam(fr,prop,'i');
                if( !result ) {
                     status = 'e';
                }
            }

            /**
             * В Таблице 24 «Встраиваемая и интернет техника», установите:
             * • поле 3 «Выброс чеков» - значение 0 (наружу),
             * • поле 4 «Выброс отчетов» - значение 1 (в ретрактор) или 0 (наружу),
             * • поле 7 «Делать петлю при печати» - значение 1 (делать).
             *  !!!!!!!!!!!!!!!!!!!!!!!! необходимо что бы у принтера в настройках был выставлен верный Paper Retracting 
             */
            result = fr->PrintReportWithCleaning();
            printf("PrintReportWithCleaning ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
            usleep(1000);
            prop->CutType = 1;
            fr->CutCheck();
            // установить флаг печати ретрактора в прежнее положение
            if( status != 'e' ) {//изменен ранее
                // установить флаг печати отчета в ретрактор(сбрасывать во внутрь)
                setPrintReportOutParam(fr,prop,status);
            }
        } else {
            // печать в буфер
            result = fr->PrintReportWithCleaningToBuffer();
            printf("PrintReportWithCleaningToBuffer ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
            usleep(1000);
        }
    }
    return result;
}

int fixLifeState(fr_func* fr, fr_prop* prop, bool fix_only_printing )
{
    // Запрос состояния ККТ
    int result = fr->GetShortStatus();
    displayResultGetShortStatus(fr, prop);
    
    if( result == 0 ){ // нет ошибок при запросе состояния ккт
        int submode = prop->ECRSubmode;
        // проверяем подрежим kkt
        if ( submode == 1 || submode == 2 || submode == 3 ) {
            printf("ERROR !!! submode %d - need try reset status!!!",submode);
        }
        
        unsigned int usec = 100000 * 2;// 1000 - 1мсек.
        
        switch( submode ){
            case 1: // Пассивное отсутствие бумаги
                break;
            case 2:// Активное отсутствие бумаги
                break;
            case 3: // После активного отсутствия бумаги
                // продолжить печать
                result = fr->ContinuePrinting();
                printf("ContinuePrinting ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);                
                break;
            case 4:// Фаза печати операции полных фискальных отчетов
                usec = 100000 * 10;
            case 5:// Фаза печати операции
                printf("\n --- wait %lf sec because submode[%d] - printing operation phase --- \n",((double)usec/1000.0/1000.0),submode);
                usleep(usec);
                break;
        }
    }
    
    if( result == -2 ) {
        perror("Could not connect");
        return result;
    }
    
    // Запрос состояния ККТ
    result = fr->GetShortStatus();
    displayResultGetShortStatus(fr, prop);

    if( result == 0 ){ // нет ошибок при запросе состояния ккт
        int mode = prop->ECRMode;
        
        // проверяем режим ккт
        if( mode == 1 || mode == 3 || mode == 6 || mode == 8 || mode == 10 ){
            printf("ERROR !!! mode %i - need try reset status!!!",mode);
        }
        switch( mode ){
            case 1:
                // 03h - Прерывание выдачи данных - InterruptDataStream
                result = fr->InterruptDataStream();
                printf("InterruptDataStream ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                break;
            case 3:
                // закрываем смену
                if( !fix_only_printing ){
                    result = AutoPrintReportWithCleaning(fr, prop, __glb_args.autoz);
                }
                break;
            case 6:
                // 23h - Подтверждение программирования даты ConfirmDate
                result = fr->ConfirmDate();
                printf("ConfirmDate ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                break;
            case 8:
                // 88h - Аннулирование чека - CancelCheck
                result = fr->CancelCheck();
                printf("CancelCheck ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                break;
            case 10:
                // 2Bh - Прерывание тестового прогона - InterruptTest
                result = fr->InterruptTest();
                printf("InterruptTest ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                break;

        }
    }

    if( result == -2 ) {
        perror("Could not connect");
    }
    
    return result;
}

unsigned int execute_command(fr_func* fr, fr_prop* prop, const char *cmd) 
{
    int result = 0;
    //LOG4CXX_DEBUG( __glb_logger, " execute_command " << "cmd = " << cmd );
    if (strcmp(cmd, "z") == 0) {
        //std::cout << "    cmd z" << std::endl;
        __glb_args.autoz = 0; // отключаем снятие отчета автоматически
        fixLifeState(fr, prop, true);
        fr->PrintReportWithCleaning();
        printf("PrintReportWithCleaning ----> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        usleep(1000);
        prop->CutType = 1;
        fr->CutCheck();
        return 1;
    } else if (strcmp(cmd, "zb") == 0) {
        //std::cout << "    cmd zb" << std::endl;
        //__glb_args.autoz = 0; // отключаем снятие отчета автоматически
        // проверяем состояние фискальника и если возможно переводим в рабочее состояние
        // без закрытия смены
        fixLifeState(fr, prop, true);
        fr->PrintReportWithCleaningToBuffer();
        printf("PrintReportWithCleaningToBuffer ----> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        usleep(1000);
        prop->CutType = 1;
        fr->CutCheck();
        return 1;
    } else if (strcmp(cmd, "x") == 0) {
        //std::cout << "    cmd x" << std::endl;
        //__glb_args.autoz = 0; // отключаем снятие отчета автоматически
        // проверяем состояние фискальника и если возможно переводим в рабочее состояние
        // без закрытия смены
        fixLifeState(fr, prop, true);
        fr->PrintReportWithoutCleaning();
        usleep(1000);
        prop->CutType = 1;
        fr->CutCheck();
        printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    } else if (strcmp(cmd, "s") == 0) {
        //std::cout << "    cmd s" << std::endl;
        fr->GetShortStatus();
        displayResultGetShortStatus(fr, prop);
       
        printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    } else if (strcmp(cmd, "b") == 0) {
        //std::cout << "    cmd b" << std::endl;
        return 1;
    } else if (strcmp(cmd, "Test") == 0) {
        //std::cout << "    cmd Test" << std::endl;
        prop->RunningPeriod = 1;
        fr->Test();
        printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    } else if (strcmp(cmd, "EndTest") == 0) {
        //std::cout << "    cmd EndTest" << std::endl;
        prop->RunningPeriod = 0;
        fr->InterruptTest();
        printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    } else if (strcmp(cmd, "S") == 0) {
        //std::cout << "    cmd S" << std::endl;
        fr->GetECRStatus();
        displayResultGetECRStatus(fr, prop);
        printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    } else if (strcmp(cmd, "f") == 0) {
        //std::cout << "    cmd f" << std::endl;
        fr->GetFNStatus();
        displayResultGetFNStatus(fr, prop);
        printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    } else if (strcmp(cmd, "c") == 0) {
       // std::cout << "    cmd c" << std::endl;
        prop->CutType = 1;
        fr->CutCheck();
        return 1;
    } else if (strcmp(cmd, "m") == 0) {
        fr->GetDeviceMetrics();
        displayResultGetDeviceMetrics(fr, prop);
        printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    } else if (strstr(cmd, "img=")!=NULL) {
        //std::cout << "    cmd img=" << std::endl;
        std::vector<const char *> cont;
        std::stringstream ss(cmd);
        std::string token;
        while (std::getline(ss, token, '=')) {
            cont.push_back(token.c_str());
        }

        if (strlen(cont[1]) > 0) {
            prop->BmpImage = (char *) cont[1];
            fr->PrintBmpImage();
            printf("%s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
            prop->CutType = 1;
            fr->CutCheck();
        }
        return 1;
    } else if (strcmp(cmd, "sync-time") == 0){ // синхронизировать дату и время
        //std::cout << "    cmd sync-time" << std::endl;
        std::time_t t = std::time(0);   // get time now
        std::tm* now = std::localtime(&t);
        std::cout << " Try set date time to " 
            << (now->tm_year + 1900) << '-' 
            << (now->tm_mon + 1) << '-' 
            <<  now->tm_mday << " "
            << now->tm_hour << ":"
            << now->tm_min << ":"
            << now->tm_sec << "\n";

        prop->Date.tm_mday = now->tm_mday;
        prop->Date.tm_mon = now->tm_mon;
        prop->Date.tm_year = now->tm_year;
        //fr.Date.tm_year - 100;
        fr->SetDate();
        printf("SetDate - %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        
        fr->ConfirmDate();
        printf("ConfirmDate ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        
        prop->Time.tm_sec = now->tm_sec;
        prop->Time.tm_min = now->tm_min;
        prop->Time.tm_hour = now->tm_hour;
        fr->SetTime();
        printf("SetTime - %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);

        return 1;
    } else if(strstr(cmd, "field-value=")!=NULL) {
        //std::cout << "    cmd field-value" << std::endl;
        std::vector<const char *> cont;
        std::stringstream ss(cmd);
        std::string token;
        while (std::getline(ss, token, '=')) {
            cont.push_back(token.c_str());
        }

        if (strlen(cont[1]) > 0) {
            (char *) cont[1];

            int sc = scanf("%d|%d|%d",&prop->TableNumber,&prop->RowNumber,&prop->FieldNumber);
            if( sc != 3 ){
                
                return 0;
            }
            
            result = fr->ReadTable();
            printf("ReadTable ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
            
            printf("type %s\n", prop->FieldType==1?"CHAR":"INT");
            printf("size %i\n", prop->FieldSize);
            if(prop->FieldType == 1){
                printf("value %s\n",prop->ValueOfFieldString);
            } else {
                printf("value %i\n", prop->ValueOfFieldInteger);
            }
            
        } else {
            printf("ReadTable ---> set all parameters[code -100]\n");
        }
        return 1;
    } else if(strcmp(cmd, "buffer-size") == 0) {
        //std::cout << "    cmd buffer-size" << std::endl;
        fr->GetBufferSize();
        printf("size %u\n", prop->BufferSize);
        printf("max-block-size %u\n", prop->BufferBlockMaxSize);
        printf("GetBufferSize - %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
        return 1;
    }
    
    std::vector<const char *> cont;
    std::stringstream ss(cmd);
    std::string token;
    while (std::getline(ss, token, '=')) {
        cont.push_back(token.c_str());
    }

    if( strlen(cont[0]) > 0 ) {
       KKTAPI::init_map_methods();
       std::cout << KKTAPI::api_method(cont[0],fr,prop,cont[1]);
       return 1;
    }
    
    return 0;
}

void init_log() {
    try
    {
        //log4cxx::PropertyConfigurator::configure("./shtrih-m-log.properties");
    }
    catch( ... )
    {
        std::cerr<<"Exception occured while opening ./shtrih-m-log.properties\n";
    }
}

int main(int argc, char** argv) 
{
    
    init_log();
    
    //LOG4CXX_DEBUG(__glb_logger, "------------------------[ main ]-------------------------------");

    prepare_arguments(argc, argv);
    create_map_commands();
    fr_func* fr;
    fr_prop* prop;
    //LOG4CXX_DEBUG(__glb_logger, "    START drvfrInitialize" );
    fr = drvfrInitialize();

    if (fr == NULL) {
        //LOG4CXX_ERROR(__glb_logger, "      FAIL" );
        perror("Could not initialize");
        return 1;
    }

    //LOG4CXX_DEBUG(__glb_logger, "      SUCCESS" );

    prop = fr->prop;
    
    //LOG4CXX_DEBUG(__glb_logger, "    START fr->Connect" );
    if (fr->Connect() == -1) {
        //LOG4CXX_ERROR(__glb_logger, "      FAIL" );
        perror("Could not connect");
        return 0xfe;
    }
    //LOG4CXX_DEBUG(__glb_logger, "      SUCCESS" );

    //LOG4CXX_DEBUG(__glb_logger, "    __glb_args.cmd = " << __glb_args.cmd );
    
    // TODO не выполнять если нет печати или есть команда, но не печати
    // проверяем состояние фискальника и если возможно переводим в рабочее состояние
    // без закрытия смены
    unsigned int result = fixLifeState(fr, prop, true);
    // сохраняем параметры "куда печатать" чеки и отчеты если изменены во входных параметрах
    if(__glb_args.out_receipt!='e'){
        result = storePrintReceiptOutParam(fr, prop,__glb_args.out_receipt_saved);
    }
    if(__glb_args.out_report!='e'){ 
        result = storePrintReportOutParam(fr, prop,__glb_args.out_report_saved);
    }
    // устанавливаем куда будут выходить чеки и отчеты исходя из настроек out_receipt и out_report
    result = setPrintOutParams(fr, prop);
    
    /*
    printf("#############################################\n");
    fr->GetPrintReceiptToOut();
    printf("GetPrintReceiptToOut ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    fr->GetPrintReportToOut();
    printf("GetPrintReportToOut ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
    printf("#############################################\n");
    */
    
    if ( __glb_args.cmd != NULL && strlen(__glb_args.cmd) > 0) {
        //LOG4CXX_DEBUG(__glb_logger, "      START execute command from arguments" );
        result = execute_command(fr, prop, __glb_args.cmd);
        //LOG4CXX_DEBUG(__glb_logger, "      END execute command from arguments" );
        // проверяем состояние фискальника и если возможно переводим в рабочее состояние
        // без закрытия смены
        result = fixLifeState(fr, prop, true);
        // возвращение параметров "куда печатать" чеки и отчеты
        result = unsetPrintOutParams(fr, prop);
        fr->DisConnect();
        if (result == 1) {
            return 0;
        }
        return result;
    }
    
    // проверяем состояние фискальника и если возможно переводим в рабочее состояние
    // с закрытием смены и фискализация с полным гашением если установлен atoz
    result = fixLifeState(fr, prop, false);
    // 
    Byte *inbuf = new Byte[MAX_LEN];
    Byte *tmp;
        
    int more_read = 1;
    bool notKKTCmd = true;
    
    // TODO for parse use finite state machine ;)
    while (more_read) {
        char c = 0;
        memset(inbuf, 0, MAX_LEN);
        tmp = inbuf;
        int buflen = 0;

        while (c != '\n') {
            int k = fread(&c, 1, 1, stdin);
            if (!k) {
                c = '\n';
                more_read = 0;
            }
            else {
                *tmp = c;
                buflen++;
            }
            tmp++;
        }
        notKKTCmd = true;
        if (buflen >= 2 && (27 == *inbuf)) {
            //LOG4CXX_DEBUG(__glb_logger, "inbuf[1]: " << inbuf[1] );
            printf("[%d] %s",buflen,inbuf);
            switch (inbuf[1]) { 
                case 'O':
                {
                    //LOG4CXX_DEBUG(__glb_logger, "inbuf[2]: " << inbuf[2] );
                    // TODO test this
                    switch (inbuf[2]) {
                        case 'p':// open Check
                        {
                            notKKTCmd = false;
                            prop->CheckType=0;
                            // TODO проверить открыта ли смена
                            fr->OpenCheck();
                            printf("OpenCheck ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                            break;
                        }
                        /*case 't':{// устанавливаем печать в ретрактор - 1 или наружу
                     /**
                     * В Таблице 24 «Встраиваемая и интернет техника», установите:
                     * • поле 3 «Выброс чеков» - значение 0 (наружу),
                     * • поле 4 «Выброс отчетов» - значение 1 (в ретрактор) или 0 (наружу),
                     * • поле 7 «Делать петлю при печати» - значение 1 (делать).
                     * // Номер таблицы,Ряд,Поле,Размер поля,Тип поля,Мин. значение, Макс.значение, Название,Значение
                     * 24,1,3,1,0,0,2,'Выброс чеков','0'
                     * 24,1,4,1,0,0,1,'Выброс отчетов','0'
                     *//*
                            
                            notKKTCmd = false;
                            
                            unsigned int out_type = 0;
                            char str[4];                            
                            int sc = sscanf((char *) inbuf + 3, "%s %u", str, &out_type);
                            if (sc != 2) {
                                //LOG4CXX_ERROR( __glb_logger, "Error set parameters in template" );
                                printf("Error set parameters in template\n");
                                break;
                            }
                            
                            if ( out_type >= 1 ){
                                prop->ValueOfFieldInteger = 1;
                            } else {
                                prop->ValueOfFieldInteger = 0;
                            }
                            if( strcmp(str, "rc") == 0 ){
                                fr->SetPrintReceiptToOut();
                            } else if( strcmp(str, "rp") == 0 ){
                                fr->SetPrintReportToOut();
                            }
                        }*/
                        case '\n':{ // Открытие смены
                            // TODO проверить выбрасывается ли чек при открытии смены
                            // возможно сделать функцию автооткрытие смены
                            notKKTCmd = false;
                            fr->GetFNStatus();
                            printf("GetFNStatus ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                            if (prop->ShiftStatus == 0) {
                                fr->CancelPrint();
                                printf("CancelPrint ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                                fr->OpenShift();
                                printf("OpenShift ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                                sleep(2);
                            }

                            if (prop->ResultCode == 0x1e) {
                                //LOG4CXX_ERROR(__glb_logger, "!!! Needs continue printing command -sending it !!!");
                                printf("ERROR !!! Needs continue printing command -sending it !!!");
                                fr->ContinuePrinting();
                                printf("ContinuePrinting ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                            }
                        }
                    }
                    break;
                }
                case 'C':
                {
                    // TODO test this
                    //LOG4CXX_DEBUG(__glb_logger, "inbuf[2]: " << inbuf[2] );
                    switch (inbuf[2]) {
                        case 'l':// close check
                        {
                            notKKTCmd = false;
                            // TODO если чек не открывался, то и не может быть закрыт
                            // TODO если были продажы, то fr->CheckSubTotal();
                            fr->CloseCheck();
                            printf("CloseCheck ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                            break;
                        }
                    }
                    break;
                }
                case 'Q':
                {
                    // TODO test this
                    //LOG4CXX_DEBUG(__glb_logger, "inbuf[2]: " << inbuf[2] );
                    switch (inbuf[2]) {
                        case 'r':// print qr code
                        {
                            notKKTCmd = false;
                            char str[MAX_LEN];
                            memset(str, 0, MAX_LEN);
                            //memcpy(str, inbuf + 3, buflen - 3);
                            unsigned int qr_version = 40, qr_mask = 8, qr_dot_size = 8, qr_align=1,qr_err_correct_level=3;
                            int sc = sscanf((char *) inbuf + 3, "%u %u %u %u %u %s", &qr_version, &qr_mask, &qr_dot_size, &qr_err_correct_level, &qr_align, str);
                            if (sc != 6) {
                                //LOG4CXX_ERROR( __glb_logger, "Error set parameters in template" );
                                printf("Error set parameters in template\n");
                                break;
                            }
                            prop->BarcodeParameter1 = qr_version;
                            prop->BarcodeParameter2 = qr_mask;
                            prop->BarcodeParameter3 = qr_dot_size;
                            prop->BarcodeParameter4 = 0;
                            prop->BarcodeParameter5 = qr_err_correct_level;
                            prop->BarCodeAlignment  = qr_align;
                            prop->BarCode = str;
                            fr->PrintQRCode();
                            printf("PrintQRCode %s ---> %s[code %d]\n", str, prop->ResultCodeDescription, prop->ResultCode);
                            break;
                        }
                    }
                    break;
                }
                case 'R':// sale cash
                {
                    notKKTCmd = false;
                    char field[MAX_LEN];
                    unsigned int department = 0;
                    unsigned int rub = 0, cop = 0;

                    int sc = sscanf((char *) inbuf + 2, "%s %u %u.%u", field, &department, &rub, &cop);
                    if (sc < 4) {
                        //LOG4CXX_ERROR( __glb_logger, "Error set parameters in template" );
                        printf("Error set parameters in template\n");
                        break;
                    }
                    double price = 1.0 * (double) rub + 1.0 * (double) cop / 100.0;

                    result = fr->OpenCheck();
                    printf("OpenCheck ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    /*
                    if( prop->ResultCode == 80 ){// 80 - Идет печать результатов выполнения предыдущей команды
                        printf("ERROR !!! Needs wait when pinting result for previos command - sleep 2 sec!!!\n");
                        sleep(1);
                    } else if( prop->ResultCode == 88 ){// 88 - Ожидание команды продолжения печати
                        printf("ERROR !!! Needs continue printing command -sending it !!!\n");
                        result = fr->ContinuePrinting();
                        printf("ContinuePrinting ---> %s[code %d]%d\n", prop->ResultCodeDescription, prop->ResultCode,result);
                    }
                    */
                    prop->Quantity = 1;
                    prop->Price = price;
                    prop->StringForPrinting = field;
                    //prop->Tax1 = 0;
                    fr->Sale();
                    printf("Sale ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    fr->CheckSubTotal();
                    printf("CheckSubTotal ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    prop->StringForPrinting = (char *)"";
                    prop->Summ1 = price;
                    fr->CloseCheck();
                    printf("CloseCheck ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    std::cout << prop->Link << "\n";
                    break;
                }
                case 'S':
                {
                    // TODO test this
                    //LOG4CXX_DEBUG(__glb_logger, "inbuf[2]: " << inbuf[2]);
                    switch (inbuf[2]) {
                            // TODO test this
                        case 'l':// sale without open and close receipt
                        {
                            notKKTCmd = false;
                            char field[MAX_LEN];
                            unsigned int department = 0;
                            unsigned int rub = 0, cop = 0;

                            int sc = sscanf((char *) inbuf + 3, "%s %u %u.%u", field, &department, &rub, &cop);
                            if (sc < 4) {
                                //LOG4CXX_ERROR( __glb_logger, "Error set parameters in template" );
                                printf("Error set parameters in template\n");
                                break;
                            }
                            double price = 1.0 * (double) rub + 1.0 * (double) cop / 100.0;

                            //fr->OpenCheck();
                            prop->Quantity = (double) department;
                            prop->Price = price;
                            prop->StringForPrinting = field;
                            //prop->Tax1 = 0;
                            fr->Sale();
                            printf("Sale ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                            //fr->CheckSubTotal();
                            //prop->StringForPrinting = "";
                            //prop->Summ1 = price;
                            //fr->CloseCheck();
                            //cout << prop->Link << "\n";
                            break;
                        }
                        case 'b':// 
                        {
                            notKKTCmd = false;
                            fr->CheckSubTotal();
                            printf("CheckSubTotal ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                            break;
                        }
                        default:
                        {
                            notKKTCmd = false;
                            unsigned int cut_type = 1;
                            int sc = sscanf((char *) inbuf + 2, "%u", &cut_type);
                            if (sc == 1) {
                                if(cut_type > 1 || cut_type < 0){
                                    cut_type = 1;
                                }
                            }
                            prop->CutType = cut_type;
                            fr->CutCheck();
                            printf("CutCheck[%u] ---> %s[code %d]\n", cut_type, prop->ResultCodeDescription, prop->ResultCode);
                        }
                    }
                    break;
                }
                case 'T':
                {
                    // TODO test this

                        //LOG4CXX_DEBUG(__glb_logger, "inbuf[2]: " << inbuf[2]);
                        switch (inbuf[2]) {
                            case 'x':// select number for tax value
                            {
                                notKKTCmd = false;
                                unsigned int value = 0, on = 0;
                                int sc = sscanf((char *) inbuf + 3, "%u %u", &value, &on);
                                printf("Set Tax%u to %u\n", value, on);
                                if (on > 0) {
                                    on = 1;
                                } else {
                                    on = 0;
                                }
                                if (value > 0 && value <= 4) {
                                    switch (value) {
                                        case 1:
                                            prop->Tax1 = on;
                                            break;
                                        case 2:
                                            prop->Tax2 = on;
                                            break;
                                        case 3:
                                            prop->Tax3 = on;
                                            break;
                                        case 4:
                                            prop->Tax4 = on;
                                            break;
                                    }
                                }
                                break;
                            }
                            default:
                            {
                                notKKTCmd = false;
                                char field[MAX_LEN];
                                unsigned int department = 0;
                                unsigned int rub = 0, cop = 0;

                                int sc = sscanf((char *) inbuf + 2, "%s %u %u.%u", field, &department, &rub, &cop);
                                if (sc < 4) {
                                    //LOG4CXX_ERROR( __glb_logger, "Error set parameters in template" );
                                    printf("Error set parameters in template\n");
                                    break;
                                }
                                double price = 1.0 * (double) rub + 1.0 * (double) cop / 100.0;
                                fr->OpenCheck();
                                printf("OpenCheck ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                                prop->Quantity = 1;
                                prop->Price = price;
                                prop->StringForPrinting = field;
                                //prop->Tax1 = 0;
                                fr->Sale();
                                printf("Sale ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                                fr->CheckSubTotal();
                                printf("CheckSubTotal ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                                prop->StringForPrinting = (char *)"";
                                prop->Summ1 = (double) 0;
                                prop->Summ2 = (double) 0;
                                prop->Summ3 = price;
                                fr->CloseCheck();
                                printf("CloseCheck ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                                std::cout << prop->Link << "\n";
                                break;
                            }
                        }

                    break;
                }
                // TODO убрать нужно вот отсюда)
                case 'Z':
                {
                    notKKTCmd = false;
                    fr->PrintReportWithCleaning();
                    printf("PrintReportWithCleaning ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    break;
                }
                case 'X':
                {
                    notKKTCmd = false;
                    fr->PrintReportWithoutCleaning();
                    printf("PrintReportWithoutCleaning ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    break;
                }
                /* вот до сюда) */
                case 'F':
                {
                    notKKTCmd = false;
                    /*
                    char str[MAX_LEN];
                    memset(str, 0, MAX_LEN);
                    memcpy(str, inbuf + 2, buflen - 2);
                     */
                    int count_str = 0;//Количество строк
                    int sc = sscanf((char *) inbuf + 2, "%u", &count_str);
                    if( sc == 1 ) {
                        count_str = sc;
                    }
                    prop->UseReceiptRibbon = true;
                    prop->StringQuantity = count_str;
                    fr->FeedDocument();
                    printf("FeedDocument ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    break;
                }
                case 'N':
                {
                    notKKTCmd = false;
                    prop->Tax1 = 0;
                    fr->Sale();
                    printf("Sale ---> %s[code %d]\n", prop->ResultCodeDescription, prop->ResultCode);
                    break;
                }
                case 'H':
                {
                    notKKTCmd = false;
                    char str[MAX_LEN];
                    memset(str, 0, MAX_LEN);
                    memcpy(str, inbuf + 2, buflen - 3);
                    prop->BarCode = str;
                    fr->PrintBarCode128ByLine();
                    //fr->PrintBarCode();
                    printf("PrintBarCode128ByLine %s --> %s[code %d]\n", str, prop->ResultCodeDescription, prop->ResultCode);
                    break;
                }
                case 'I':
                {
                    notKKTCmd = false;
                    // TODO test this
                    //LOG4CXX_DEBUG(__glb_logger, "inbuf[2]: " << inbuf[2] );
                    switch (inbuf[2]) {
                            // TODO test this
                        case 'm':// sale without open and close receipt
                        {
                            char field[MAX_LEN];
                            unsigned int squard = 1;
                            int sc = sscanf((char *) inbuf + 3, "%u %s", &squard, field);
                            if (sc != 2) {
                                //LOG4CXX_ERROR( __glb_logger, "Error set parameters in template" );
                                printf("Error set parameters in template\n");
                                break;
                            }

                            //LOG4CXX_DEBUG(__glb_logger, " --- bmp img location" << field );
                            prop->BmpImage = field;
                            prop->BmpImageSquard = squard;
                            fr->PrintBmpImage();
                            printf("PrintBmpImage %s ---> %s[code %d]\n", field, prop->ResultCodeDescription, prop->ResultCode);
                            break;
                        }
                    }
                    break;
                }
            }
        }

        if( notKKTCmd ){
            //TODO max len of string 38 if more then print on next
            char str[MAX_LEN];
            memset(str, 0, MAX_LEN);
            if (buflen > 1) {
                bool wide=false;
                if (*inbuf == 'b') {
                    memcpy(str, inbuf + 1, buflen - 1);
                    //LOG4CXX_DEBUG(__glb_logger, "[b]");
                } else if(*inbuf == 'B') {
                    memcpy(str, inbuf + 1, buflen - 1);
                    wide=true;
                    //LOG4CXX_DEBUG(__glb_logger, "[B]");
                } else {
                    memcpy(str, inbuf, buflen);
                }
                //LOG4CXX_DEBUG(__glb_logger, ":::: print it :::: [" << str << "]" );
                prop->UseReceiptRibbon = 1;
                prop->StringForPrinting = str;
                int tires = 0;
                while( tires < 3 ){
                    if(wide){
                        result = fr->PrintWideString();
                        printf("PrintWideString %s ---> %s[code %i]%u\n", str, prop->ResultCodeDescription, prop->ResultCode,result);
                    } else {
                        result = fr->PrintString();
                        printf("PrintString %s ---> %s[code %i]%u\n", str, prop->ResultCodeDescription, prop->ResultCode,result);
                    }
                    
                    if( prop->ResultCode == 80 ){// 80 - Идет печать результатов выполнения предыдущей команды
                        #define WAIT_PRINTING_END_TIME_SEC = 1
                        printf("ERROR !!! Needs wait when pinting result for previos command - sleep 1 sec!!!\n");
                        sleep(1);
                        
                        break; // ????????????????????? mey be one mo time print
                    } else if( prop->ResultCode == 88 ){// 88 - Ожидание команды продолжения печати
                        printf("ERROR !!! Needs continue printing command -sending it !!!\n");
                        result = fr->ContinuePrinting();
                        printf("ContinuePrinting %s ---> %s[code %i]%u\n", str, prop->ResultCodeDescription, prop->ResultCode,result);
                    } else {
                        break;
                    }
                    tires++;

                }
            }
        }
    }

    // проверяем состояние фискальника и если возможно переводим в рабочее состояние
    // с закрытием смены и фискализация с полным гашением если установлен atoz
    result = fixLifeState(fr, prop, false);
    // возвращение параметров "куда печатать" чеки и отчеты
    result = unsetPrintOutParams(fr, prop);
    
    delete(inbuf);

    fr->DisConnect();
}