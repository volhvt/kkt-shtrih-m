/***************************************************************************
                          options.h  -  description
                             -------------------
    begin                : Fri Jan 11 2002
    copyright            : (C) 2002 by Igor V. Youdytsky
    email                : Pitcher@gw.tander.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _OPTIONS_H
#define _OPTIONS_H

#include <unistd.h>
#include <fcntl.h>

#include "conn.h"

#define MAX_LEN 1024

#define DEFAULT_PORT "/dev/ttyACM0"
#define DEFAULT_BAUDRATE 115200
#define DEFAULT_PASSWORD "30"
#define DEFAULT_TIMEOUT 50

struct GlobalArgs {
    unsigned int debug;
    unsigned int debug_level;

    
    
    char* port; // порт ККТ
    unsigned int baudrate; // скорость
    int flow; // четность
    char* password; // пароль оператора ККТ
    char* password_sc; // пароль ЦТО
    char* password_ti; // пароль налогового инспектора
    char* password_admin; // пароль системного администратора
    unsigned int timeout; // время в мили секундах для отправки одного байта
    
    unsigned int use_auto_timeout; // Использовать увеличение таймаута
    unsigned int step_auto_timeout; // Шаг увеличения таймаута 
    unsigned int max_auto_timeout; // Величина до которой можно увеличить автоматически(при неудачи подключения к устройству или не выполнении команды) таймаут для отправки 1 байта.
    
    
    unsigned int max_connect_tries; // кол-во попыток при отправке ENQ
    unsigned int max_execute_tries; // кол-во повторных поппыток выполнить комманду в случае ошибок соединения(устройство ответило)
    unsigned int max_resend_tries; // кол-во повторных поппыток отправить команду
    unsigned int max_tries; // кол-во попыток отправить/получить данные от устройства

    unsigned char out_receipt;// куда направлять печать чеков по умолчанию( e - не задавался, i - в ретрактор, o - наружу ), может быть изменен командой в чеке {ESC}Ot rc 1 - в ретрактор || {ESC}Ot rc 0 - наружу
    unsigned char out_report;// куда направлять печать отчетов по умолчанию( e - не задавался, i - в ретрактор, o - наружу ), может быть изменен командой в чеке {ESC}Ot rp 1 - в ретрактор || {ESC}Ot rp 0 - наружу    
    unsigned char out_receipt_saved;//сохраненные значения из настроек фискальника( e - не задавался, i - в ретрактор, o - наружу )
    unsigned char out_report_saved;//сохраненные значения из настроек фискальника( e - не задавался, i - в ретрактор, o - наружу )
    
    unsigned int autoz; // автоматически закрывать смену, если время уже истекло
    unsigned int cut_off; // отправить команду для отрезки чека
    char* cmd; // команда с параметрами для отправки на ККТ

    GlobalArgs(){
        this->debug = 0;
        this->debug_level = 0;
        this->port = (char *)DEFAULT_PORT;
        this->baudrate = DEFAULT_BAUDRATE;
        this->flow = 0;
        this->password = (char *)DEFAULT_PASSWORD;
        this->password_sc = (char *)DEFAULT_PASSWORD;
        this->password_ti = (char *)DEFAULT_PASSWORD;
        this->password_admin = (char *)DEFAULT_PASSWORD;
        this->out_receipt = 'e';
        this->out_report = 'e';
        this->out_receipt_saved = 'e';
        this->out_report_saved = 'e';
        this->autoz = 0;
        this->cut_off = 0;
        this->cmd = (char *)"";
        this->timeout = DEFAULT_TIMEOUT;
        this->use_auto_timeout = USE_AUTO_TIMEOUT;
        this->step_auto_timeout = STEP_AUTO_TIMEOUT;
        this->max_auto_timeout = MAX_AUTO_TIMEOUT;
        this->max_connect_tries = MAX_CONNECT_TRIES;
        this->max_execute_tries = MAX_EXECUTE_TRIES;
        this->max_resend_tries = MAX_RESEND_TRIES;
        this->max_tries = MAX_TRIES;
        
    }
};

extern struct GlobalArgs __glb_args;

static const char *LineSpeed[] ={
    "2400",
    "4800",
    "9600",
    "19200",
    "38400",
    "57600",
    "115200",
    "230400",
    "460800",
    "921600"
};

int get_num_baudrate(int bValue);

int readoptions(void);

#endif
